bareos (16.2.6-5~1.gbp811295) UNRELEASED; urgency=medium

  ** SNAPSHOT build @8112958ea27f9258402b7b6c18b82f38d6f8f49e **

  * Fix fadvise POSIX_FADV_DONTNEED not being called.

 -- Anthony DeRobertis <anthony@derobert.net>  Mon, 08 Oct 2018 13:28:39 -0400

bareos (16.2.6-4) unstable; urgency=medium

  * Work around problems with the installation order of the postgresql
    autopkgtest.

 -- Felix Geyer <fgeyer@debian.org>  Tue, 15 May 2018 17:35:57 +0200

bareos (16.2.6-3) unstable; urgency=medium

  * Remove duplicate config check call in the init script.
    - Avoids (harmless) warning when /etc/bareos/bareos-dir.conf doesn't exist.
  * Fix permissions of bareos-dir logrotate config on upgrade. (Closes: #864926)

 -- Felix Geyer <fgeyer@debian.org>  Mon, 31 Jul 2017 00:07:17 +0200

bareos (16.2.6-2) unstable; urgency=medium

  * Fix file corruption when using SHA1 signature. (Closes: #869608)
  * Add autopkgtest for SHA1 signature.

 -- Felix Geyer <fgeyer@debian.org>  Thu, 27 Jul 2017 19:50:17 +0200

bareos (16.2.6-1) unstable; urgency=medium

  * New upstream release.
  * Drop d/patches/disable-fstype-test, fixed upstream.
  * Fix mysql autopkgtest.
    -  Reinstall database in case mysql wasn't started early enough.

 -- Felix Geyer <fgeyer@debian.org>  Thu, 29 Jun 2017 23:51:24 +0200

bareos (16.2.5-2) unstable; urgency=medium

  * Upload to unstable.

 -- Felix Geyer <fgeyer@debian.org>  Sun, 18 Jun 2017 10:02:36 +0200

bareos (16.2.5-1) experimental; urgency=medium

  * Team upload to experimental.
  * New upstream release.
  * Make bareos-client only suggest bareos-traymonitor. (Closes: #854109)

 -- Felix Geyer <fgeyer@debian.org>  Thu, 20 Apr 2017 18:42:58 +0200

bareos (16.2.4-3) unstable; urgency=medium

  * Team upload.

  [ Evgeni Golov ]
  * remove myself from uploaders
  * mark bareos-filedaemon-ceph-plugin as linux-any

  [ Felix Geyer ]
  * Re-add accidentally dropped bareos-bat package.

 -- Felix Geyer <fgeyer@debian.org>  Sun, 08 Jan 2017 18:36:30 +0100

bareos (16.2.4-2) unstable; urgency=medium

  * Team upload to unstable.
  * Limit the package build to 4 parallel jobs. The upstream build system
    isn't able to handle more. (Closes: #844136)

 -- Felix Geyer <fgeyer@debian.org>  Sun, 27 Nov 2016 12:35:23 +0100

bareos (16.2.4-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

  [ Evgeni Golov ]
  * use default-mysql packages
  * fix typo: additonal → additional
  * use https url in Vcs-Git
  * Standards-Version: 3.9.8

  [ Felix Geyer ]
  * Refresh dont-generate-debian-files patch
  * Update tests for new config file locations
  * Switch back to the default gcc 6.

 -- Felix Geyer <fgeyer@debian.org>  Sun, 06 Nov 2016 13:12:30 +0100

bareos (15.2.4-2) unstable; urgency=medium

  * Team upload.
  * Build wth gcc 5 for now. (Closes: #840488)
    - bareos 15.2 is not compatible with optimizations from gcc 6,
      see https://bugs.bareos.org/view.php?id=662#c2365
  * Disable the fstype test as it's broken in some chroot configurations.

 -- Felix Geyer <fgeyer@debian.org>  Sun, 16 Oct 2016 22:51:21 +0200

bareos (15.2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Fixes a bug when mounting disks by the storage daemon. (Closes: #823344)

 -- Felix Geyer <fgeyer@debian.org>  Sun, 03 Jul 2016 13:43:52 +0200

bareos (15.2.3-2) unstable; urgency=medium

  * Team upload to unstable.
  * Enable all hardening build flags.

 -- Felix Geyer <fgeyer@debian.org>  Thu, 21 Apr 2016 23:16:15 +0200

bareos (15.2.3-1) experimental; urgency=medium

  [ Felix Geyer ]
  * Add autopkgtests for TLS.
  * Add breaks-testbed to all tests.

  [ Evgeni Golov ]
  * New upstream release.
  * only change fileperms for files that exist (Closes: #808580)

 -- Evgeni Golov <evgeni@debian.org>  Thu, 07 Apr 2016 21:18:59 +0200

bareos (15.2.2-2) experimental; urgency=medium

  * add proper breaks/conflics between bareos-storage and b-s-tape
    (Closes: #808311)
  * yank out univention support, upstream needs it, we do not
  * drop most Ubuntu 8.04 workarounds
  * only build ceph and gluster support on Linux
  * update homepage and source repo URLs for LZ4

 -- Evgeni Golov <evgeni@debian.org>  Sat, 19 Dec 2015 17:17:37 +0100

bareos (15.2.2-1) experimental; urgency=medium

  * New upstream release.

  [ Marco van Wieringen ]
  * First plugin for doing LDAP backups.

  [ Joerg Steffens ]
  * plugin-python-ldap: add example configuration file
  * CEPH packages for Debian based distributions
  * Cephfs-, Rados- and Gfapi-plugins: added examples

  [ André Bauer ]
  * build glusterfs plugins on Debian/Ubuntu

  [ Evgeni Golov ]
  * refresh patches against 15.2.1
  * add CEPH packages
  * improve package descriptions
  * allign the first sentence of the metapackage description with the others
  * indent Desciption of bareos-common as the other descriptions
  * drop openssl-no-home-rnd, applied upstream
  * drop set-dbuser-dbname, applied upstream
  * use dh-systemd unconditionally
  * build depend on libcmocka-dev (>= 1.0.1)
  * Build-Depends on libjansson-dev
  * drop unneeded debian/bareos-filedaemon-python-plugin.dir
  * reorder packages in d/conrol to match upstream

 -- Evgeni Golov <evgeni@debian.org>  Sun, 13 Dec 2015 09:13:31 +0100

bareos (14.2.6-1) unstable; urgency=medium

  * New upstream release.

  [ Joerg Steffens ]
  * logrotate: added build requirement

  [ Marco van Wieringen ]
  * Fix some packaging problems.

  [ Evgeni Golov ]
  * move bscrypto binary to bareos-storage-tape
  * do not use dh-exec in bareos-storage.install

 -- Evgeni Golov <evgeni@debian.org>  Sat, 12 Dec 2015 20:21:25 +0100

bareos (14.2.5-2) unstable; urgency=medium

  * call openssl rand with an excplicit RANDFILE=/dev/urandom
  * call the main license "Bareos" instead of "AGPL with exceptions"
  * exclude plugins and backends from shlibs
  * re-order files sections in d/copyright to fix "unused file" warning
  * explicitly install only /usr/share/doc/bareos/html/bat/ in bareos-bat
  * add doc-base integration for bareos-bat docs
  * build-depend on libcap-dev only on linux-any
  * disable acl, xattr, scsi-crpyto and ndmp on kFreeBSD
  * add gbp.conf
  * build-depend on dh-exec
  * only install ndmp and scsi-crypto files on linux-any
  * try to compile on GNU/Hurd too
  * don't use [0-9] in debian/*.install

 -- Evgeni Golov <evgeni@debian.org>  Sun, 11 Oct 2015 18:01:16 +0200

bareos (14.2.5-1) unstable; urgency=medium

  * New upstream release.
    + Fixes corruption when using multi-volume disk-based jobs
      Closes: #788543

  [ Joerg Steffens ]
  * fixes some warnings from qa.debian.org
  * set Debian section to python for Python packages
  * fixes usage of dbconfig-common when installing
  * fixes migrating from psql with password
  * fixes a problem when migrating to dbconfig
  * changed package dependencies
    Closes: #771870

  [ Marco van Wieringen ]
  * Add missing bpipe fd plugin to filedaemon pkg.
    Closes: #784923

  [ Felix Geyer ]
  * add autopkgtests
    Closes: #788587

  [ Evgeni Golov ]
  * merge back changes from debian on top of the new branch
  * Standards-Version: 3.9.6
  * update d/copyright
  * use debhelper 9
  * do not set DH_VERBOSE / DH_OPTIONS=-v
  * do not generate files in debian/ by upstream scripts
  * also set dbuser and dbname in apply_dbconfig_settings()
  * Switch Git URL to Alioth

 -- Evgeni Golov <evgeni@debian.org>  Sun, 02 Aug 2015 19:46:11 +0200

bareos (14.2.1+20141017gitc6c5b56-4) unstable; urgency=medium

  [ Joerg Steffens ]
  * clearified description for bareos-database-common.
    Thanks to Davide Prina (Closes: #768606)
  * on purge, also remove /etc/bareos/.rndpwd.
    Thanks to Andreas Beckmann (Closes: #769096)
  * get rid of circular dependencies.
    Thanks to Bill Allombert (Closes: #769536)

 -- Evgeni Golov <evgeni@debian.org>  Tue, 02 Dec 2014 10:23:53 +0100

bareos (14.2.1+20141017gitc6c5b56-3) unstable; urgency=medium

  * do not install files as conffiles in /etc/bareos (Closes: #767819)

 -- Evgeni Golov <evgeni@debian.org>  Tue, 04 Nov 2014 07:19:30 +0100

bareos (14.2.1+20141017gitc6c5b56-2) unstable; urgency=medium

  * Add more conflicts against Bacula packages.
    Thanks to Ralf Treinen <treinen@free.fr> (Closes: #767058)

 -- Evgeni Golov <evgeni@debian.org>  Thu, 30 Oct 2014 17:17:58 +0100

bareos (14.2.1+20141017gitc6c5b56-1) unstable; urgency=medium

  * Import newer upstream snapshot.
  * Use GnuTLS instead of OpenSSL to avoid licensing problems.
  * Don't generate debian/control during build.

 -- Evgeni Golov <evgeni@debian.org>  Sat, 25 Oct 2014 21:32:39 +0200

bareos (14.2.1+20140930git1339225-1) unstable; urgency=medium

  * Initial Debian release. (Closes: Bug#762459)

 -- Evgeni Golov <evgeni@debian.org>  Wed, 08 Oct 2014 13:21:16 +0200
